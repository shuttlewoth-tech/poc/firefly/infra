terraform {
  backend "http" {
  }
}

locals {
  parent_org_id   = "978534185875"
  billing_account = "014C9A-717A52-864BF1"
  project_id      = "accurate-firefly"


  project_budget          = 20
  default_bucket_location = "EU"

  parent_domain = "shuttleworth.tech"

}

#module "project-factory" {
#  source  = "terraform-google-modules/project-factory/google"
#  version = "~> 14.1"
#
#  name                 = local.project_id
#  org_id               = local.parent_org_id
#  billing_account      = local.billing_account
#  domain = local.domain_name
#
#  bucket_location = local.default_bucket_location
##  usage_bucket_name    = "${local.project_id}-usage-report-bucket"
#  budget_amount = local.project_budget
#  budget_calendar_period = "MONTH"
#}